winery_management
=================

Adiczion's Tryton Module: Winery Management
-------------------------------------------

Adds the ability to manage a winery to [Tryton](http://www.tryton.org/) among the added functionality you will find:

 * Overload the products sheet for add the following informations:
   * Vintage
   * Quality
   * Grape concentration
   * Winemaking
   * alcohol
   * Harvest date

[TODO]

 * Add Excise management:
   * Overloading location to add part of excise number.
