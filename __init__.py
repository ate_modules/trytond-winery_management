# -*- coding: utf-8 -*-
# This file is part of Adiczion Wine Management Tryton Module.  The 
# COPYRIGHT file at the top level of this repository contains the
# full copyright notices and license terms.

from trytond.pool import Pool
from .company import *
from .winery import *
from .product import *
#from .location import *

def register():
    Pool.register(
        Company,
        WineQuality,
        Grape,
        GrapeConcentration,
        Template,
        Product,
#        Location,
        module='winery_management', type_='model')
#    Pool.register(
#        ExciseMove,
#        module='winery_management', type_='wizard')
