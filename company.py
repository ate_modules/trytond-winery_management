# -*- coding: utf-8 -*-
# This file is part of Adiczion Wine Management Tryton Module.  The 
# COPYRIGHT file at the top level of this repository contains the
# full copyright notices and license terms.

from trytond.pyson import Eval
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import PoolMeta


__all__ = ['Company']
#__all__ = ['Company','CompanyExciseLocation']
__metaclass__ = PoolMeta

STATES = {
    'readonly': ~Eval('active', True),
    }
DEPENDS = ['active']

class Company(ModelSQL, ModelView):
    
    __name__ = 'company.company'

    wine_category = fields.Many2One('product.category', 'Wine category',
        states=STATES, depends=DEPENDS,
        help='Category for wine products. If an article is in this category '
            'there are new fields showing in product sheet and the name of '
            'product is equal to : product name + product vintage + '
            'product quality.')
#    excise_location = fields.Many2Many('company.excise.stock.location',
#        'company', 'location', 'Excise location',
#        help='List of locations in excise. All stock movements into or out '
#            'of these locations will be counted and listed in the report to '
#            'be sent to the customs monthly.')

#class CompanyExciseLocation(ModelSQL):
#    'Company - Location'
#    __name__ = 'company.excise.stock.location'
#    _table = 'company_excise_location_rel'
#    company = fields.Many2One('company.company', 'Company', ondelete='CASCADE',
#            required=True, select=True)
#    location = fields.Many2One('stock.location', 'Location',
#        ondelete='CASCADE', required=True, select=True)

