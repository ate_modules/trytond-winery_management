.. ATM Winery Management documentation master file, created by
   sphinx-quickstart on Sat Aug 17 10:49:54 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ATM Winery Management's documentation!
=================================================

This module adds the ability to manage a cellar with Tryton ERP. To this we add the following features:

 * Add winery informations to :ref:`product`
 * Add Customs information to :ref:`location`

Contents:

.. toctree::
  :maxdepth: 2

  product
  location


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

