# -*- coding: utf-8 -*-
# This file is part of Adiczion Wine Management Tryton Module.  The 
# COPYRIGHT file at the top level of this repository contains the
# full copyright notices and license terms.

from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction
from trytond.pyson import Eval, Bool, Not
from trytond.pool import Pool, PoolMeta
from trytond.report import Report

__all__ = ['Template', 'Product', 'ProductByLocationReport']
__metaclass__ = PoolMeta

STATES = {
    'readonly': ~Eval('active', True),
    }
DEPENDS = ['active']

SEPARATOR = ' '

class Template(ModelSQL, ModelView):
    __name__ = "product.template"
   
    is_wine = fields.Boolean('Is wine',
        depends=['category'])
    category = fields.Many2One('product.category', 'Category',
        states=STATES, depends=DEPENDS, on_change=['category'] )
    quality = fields.Many2One('wine.quality', 'Quality',
        states={'invisible': Not(Bool(Eval('is_wine'))),},
        help = "Quality of wine.")
    grape_concentration = fields.One2Many('grape.concentration', 'template', 
        'Grape concentration',
        states={'invisible': Not(Bool(Eval('is_wine'))),},
        help='Distribution of different grape varieties (percent)')
   
    @classmethod
    def __setup__(cls):
        super(Template, cls).__setup__()
        cls._constraints += [
            ('check_grape_concentration', 'concentration_not_valid'),
        ]
        cls._error_messages.update({
            'concentration_not_valid': 'The sum of concentration must be ' \
                'equal to 100.0!',
        })
   
    @classmethod
    def check_grape_concentration(cls, templates):
        for template in templates:
            if template.grape_concentration:
                pct = sum([int(i.concentration) for i in \
                           template.grape_concentration])
                if pct != 100.0:
                    return False
        return True
    
    @staticmethod
    def default_is_wine():
        return False
    
    def on_change_category(self):
        User = Pool().get('res.user')
        user = User(Transaction().user)
        
        res = {}
        res['is_wine'] = False
        if self.category and \
                user.company.wine_category and \
                self.category.id == user.company.wine_category.id:
            res['is_wine'] = True
        return res
    
class Product(ModelSQL, ModelView):
    __name__ = "product.product"
        
    vintage = fields.Char('Vintage', size=4, translate=True, select=True,
        states={
            'invisible': Not(Bool(
                Eval('_parent_template', {}).get('is_wine'))),
        })
    winemaking = fields.Text("Winemaking", translate=True,
        states={
            'invisible': Not(Bool(
                Eval('_parent_template', {}).get('is_wine'))),
        },
        help='Little description of winemaking.')
    alcohol = fields.Float('Alcohol proof (%)', digits=(16, 2),
        states={
            'invisible': Not(Bool(
                Eval('_parent_template', {}).get('is_wine'))),
        })
    harvest_date = fields.Char('Harvest date', size=32, translate=True,
        states={
            'invisible': Not(Bool(
                Eval('_parent_template', {}).get('is_wine'))),
        },
        help='Period or date of harvest.')

    @staticmethod
    def default_is_wine():
        return False
    
    def get_rec_name(self, name):
         pool = Pool()
         User = pool.get('res.user')
         user = User(Transaction().user)
         pname = super(Product, self).get_rec_name(name)
         if self.category and self.category.id == user.company.wine_category.id:
             return SEPARATOR.join(filter(None, [pname, self.vintage,
                self.template.quality and self.template.quality.name]))
         return pname

class ProductByLocationReport(Report):
    __name__ = 'product.product.product_by_location'
     
    def parse(self, report, objects, data, localcontext):
        print data
        print localcontext
        res = super(ProductByLocationReport, self).parse(report, objects, data,
                localcontext)
        return res
