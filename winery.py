# -*- coding: utf-8 -*-
# This file is part of Adiczion Wine Management Tryton Module.  The 
# COPYRIGHT file at the top level of this repository contains the
# full copyright notices and license terms.

from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool, PoolMeta

__all__ = ['WineQuality', 'Grape', 'GrapeConcentration']
__metaclass__ = PoolMeta


class WineQuality(ModelSQL, ModelView):
    "Wine quality"
    __name__ = 'wine.quality'
    
    name = fields.Char('Name', size=None, required=True, translate=True,
        select=True, help="Quality of wine (Cuvee, ...).")
    description = fields.Text('Description')

class Grape(ModelSQL, ModelView):
    "Grape"
    __name__ = 'grape.grape'
    
    name = fields.Char('Name', size=None, required=True, translate=True,
        select=True, help="Name of grape.")
    description = fields.Text('Description')
    
class GrapeConcentration(ModelSQL, ModelView):
    "Grape concentration"
    __name__ = 'grape.concentration'
    _rec_name = 'concentration'

    @classmethod
    def __setup__(cls):
        super(GrapeConcentration, cls).__setup__()
        cls._order.insert(0, ('sequence', 'DESC'))
        cls._sql_constraints += [
            ('grape_uniq', 'UNIQUE(grape, template)',
                'We cannot have two identical grape in one bottle!'),
        ]
    
    template = fields.Many2One('product.template', 'Product', 
        ondelete = 'CASCADE', select = True)
    concentration = fields.Numeric('Concentration', digits = (16, 4),
        required = True,
        help = "Set the percentage concentration of the grape.")
    grape = fields.Many2One('grape.grape', 'Grape', required = True,
        help = "Select ...")
    sequence = fields.Integer('Sequence', select=True, required=True)
